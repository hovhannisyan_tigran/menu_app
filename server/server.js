﻿const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const formidable = require('express-formidable');
const dbconfig = require('./dbconfig');
const mssql = require('mssql');

const app = express();
const connection = new mssql.ConnectionPool(dbconfig);
const request = new mssql.Request(connection);

const port = process.env.PORT || 8000;
const staticURL = 'http://localhost:'+ port +'/static/';

app.use(formidable({ multiples: true }));
app.use('/static', express.static(__dirname + '/images'));
app.use(bodyParser.urlencoded({ extended: false }));

connection.connect(err => {
    if (err) throw err;
});

app.get('/api/products:categoryId', (req, response) => {
    request.query(`SELECT * From products WHERE category_id = ${req.params.categoryId}`, async (err, res) => {
        if (err) throw err;
        const { recordsets } = res;
        const products = recordsets[0];
        products.forEach((product) => {
            product.image = staticURL + product.image;
        });
        response.send({ products: recordsets[0] });
    });
});

app.post('/api/cart', (req, response) => {
    const { cart } = req.fields;
    const { tableNumber } = req.fields;
    const { address } = req.fields;
    cart.map(item => {
        request.query(`INSERT INTO orders (product_id, quantity, table_number, address) values
        (${item.id},
            ${item.quantity},
            ${tableNumber},
            N'${address}')`,
            (err) => {
                if (err) throw err;
                response.send();
            });

    })
});

const saveProduct = (product, cb) => {
    var data = product.image.replace(/^data:image\/\w+;base64,/, "");
    var ext = product.image.split(';')[0].match(/jpeg|png|gif/)[0];
    var buffer = Buffer.from(data, 'base64');
    var fileName = product.categoryId + product.title + Date.now() + '.' + ext;
    try {
        fs.writeFile('./images/' + fileName, buffer, function (err) {
            if (err) throw err;
            console.log('Saved!');
            cb(fileName);
        });
    }
    catch (e) {
        console.log(e);
    }
}

app.post('/api/admin', (req, response) => {
    const product = req.fields;
    saveProduct(product, (imageName) => {
        request.query(`INSERT INTO products (category_id, title, description, price, image) values 
                    (${product.categoryId},
                    N'${product.title}',
                    N'${product.description}',
                    ${product.price},
                    N'${imageName}')`, function (err, res) {
            response.send(res);
        });
    });
});

app.get('/api/manager', (req, response) => {
    request.query(`SELECT orders.id, products.title, products.price, products.description, orders.quantity, orders.table_number, orders.address
                    FROM orders
                    INNER JOIN products ON orders.product_id = products.id WHERE orders.is_active = 1;`, function (err, res) {
        if (err) throw err;
        var { recordset } = res;
        response.send({ orders: recordset });
    });
});

app.put("/api/manager", (req, response) => {
    request.query(`UPDATE orders SET is_active = 0 WHERE table_number = ${req.fields.tableNumber} AND address = N'${req.fields.address}';`, function (err, res) {
        if (err) throw err;
        var { recordset } = res;
        response.send({ orders: recordset });
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));