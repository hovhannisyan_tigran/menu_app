import { put } from '../../api/apiHelper';

export const FETCH_ORDERS_PENDING = 'FETCH_ORDERS_PENDING';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_ERROR = 'FETCH_ORDERS_ERROR';
export const CHANGE_ORDER_STATUS = 'CHANGE_ORsDER_STATUS';

export const fetchOrdersPending = () => {
    return {
        type: FETCH_ORDERS_PENDING
    }
}

export const fetchOrdersSuccess = (orders) => {
    return {
        type: FETCH_ORDERS_SUCCESS,
        payload: {
            orders
        }
    }
}

export const fetchOrdersError = (error) => {
    return {
        type: FETCH_ORDERS_ERROR,
        error: error
    }
}

export const changeOrderStatus = (tableNumber, address) => {
    console.log(tableNumber, address);
    put('/api/manager', { tableNumber, address });
    return {
        type: CHANGE_ORDER_STATUS,
        payload: {
            tableNumber,
            address
        }
    }
};