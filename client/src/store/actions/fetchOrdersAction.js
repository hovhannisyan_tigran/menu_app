import { get } from '../../api/apiHelper';
import { fetchOrdersPending, fetchOrdersSuccess, fetchOrdersError } from './orderActions';

const getActiveOredrs = () => {
    return dispatch => {
        dispatch(fetchOrdersPending());
        var activeOrders = [];
        get('/api/manager', (res) => {
            activeOrders = res.orders;
            dispatch(fetchOrdersSuccess(activeOrders));
        },
            (error) => {
                dispatch(fetchOrdersError(error));
                throw error;
            })
    }
}

export default getActiveOredrs;