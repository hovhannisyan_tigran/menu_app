import { get } from '../../api/apiHelper';
import { fetchProductsPending, fetchProductsSuccess, fetchProductsError } from './productActions';

const fetchProducts = (categoryId) => {
    return dispatch => {
        dispatch(fetchProductsPending());
        get(`/api/products${categoryId}`, (res) => {
            dispatch(fetchProductsSuccess(res.products));
            return res.products;
        }, (error) => {
            dispatch(fetchProductsError(error));
        })
    }
}

export default fetchProducts;