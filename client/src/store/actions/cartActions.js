import { post } from '../../api/apiHelper';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const UPDATE_CART_QUANTITY = 'UPDATE_CART_QUANTITY';
export const CONFIRM_ORDER = 'CONFIRM_ORDER';

export const addToCart = (product) => {
    return {
        type: ADD_TO_CART,
        payload: {
            product,
            quantity: 1
        }
    }
};

export const removeFromCart = (productId) => {
    return {
        type: REMOVE_FROM_CART,
        payload: {
            productId: productId
        }
    }
};

export const updateCartQuantity = (productId, quantity) => {
    return {
        type: UPDATE_CART_QUANTITY,
        payload: {
            productId,
            quantity
        }
    }
};

export const confirmOrder = (cart, tableNumber, address) => {
    console.log("/api/cart", cart, tableNumber, address);
    post('/api/cart', {
        cart,
        tableNumber,
        address
    });
    return {
        type: CONFIRM_ORDER,
        payload: {
            cart,
            tableNumber,
            address
        }
    }
};