import {
    FETCH_ORDERS_PENDING,
    FETCH_ORDERS_SUCCESS,
    FETCH_ORDERS_ERROR,
    CHANGE_ORDER_STATUS
} from '../actions/orderActions';

const initialState = {
    pending: true,
    orders: [],
    error: null
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDERS_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_ORDERS_SUCCESS:
            return {
                ...state,
                pending: false,
                orders: action.payload.orders
            }
        case FETCH_ORDERS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case CHANGE_ORDER_STATUS:
            return {
                state
            }
        
        default:
            return state;
    }
};

export const getOrders = state => state.order.orders;
export const getOrdersPending = state => state.order.pending;
export const getOrdersError = state => state.order.error;
export default orderReducer;