import productReducer from './productReducer';
import cartReducer from './cartReducer';
import orderReducer from './orderReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    product: productReducer,
    cart: cartReducer,
    order : orderReducer
});

export default rootReducer;