﻿import {
    FETCH_PRODUCTS_PENDING,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_ERROR,
    ADD_TO_CART
} from '../actions/productActions';

const initialState = {
    pending: false,
    products: [],
    error: null
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                pending: false,
                products: action.payload.products
            }
        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case ADD_TO_CART:
            let product = state.products.find(produbt => produbt.id === action.payload.product.id);
            product.inCart = true;
            return {
                ...state,
                products: state.products
            };
        default:
            return state;
    }
};

export const getProducts = state => state.products;
export const getProductsPending = state => state.pending;
export const getProductsError = state => state.error;
export default productsReducer;