import React, { Component } from 'react';
import { Form, Button, Col, Image } from 'react-bootstrap';
import { upload } from '../api/apiHelper';
import defaultImage from '../images/default.png';
import menuTree from '../data/menu';

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: menuTree,
            subcategories: menuTree[0].children
        }

    }

    handleSubmit = (event) => {
        event.preventDefault();
        let form = event.currentTarget.elements;
        let length = form.length - 1;
        let formData = new FormData();
        for (let i = 0; i < length; i++) {
            if (form[i].name) {
                formData.append(form[i].name, form[i].value);
            }
        }
        var demoImage = document.querySelector('img');
        formData.append('image', demoImage.src);
        upload('/api/admin', formData);
        event.currentTarget.reset();
        demoImage.src = defaultImage;
    };

    getSelectOptions = (list) => {
        let options = [];
        list.forEach(category => {
            options.push(
                <option key={category.id} value={category.id} >{category.name}</option>
            )
        });
        return options;
    };

    onCategoryChange = (e) => {
        let selectedOption = this.state.categories.find(item => (item.id.toString() === e.target.value));
        let subcategories = selectedOption.children;
        this.setState({ subcategories: subcategories });
    };


    onUpload = (event) => {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var file = event.target.files[0];
            var reader = new FileReader();
            var demoImage = document.querySelector('img');

            reader.onload = function (event) {
                demoImage.src = reader.result;
            }
            reader.readAsDataURL(file);
        } else {
            alert("Your browser is too old to support HTML5 File API");
        }
    };

    render() {
        return (
            <div className={'container'}>
                <h1>Ավելացնել ապրանք</h1>
                <Form id={'form'} name={'form'} onSubmit={this.handleSubmit}>
                    <Form.Row>
                        <Form.Group as={Col} controlId={'title'}>
                            <Form.Label>Անուն</Form.Label>
                            <Form.Control required type={'text'} name={'title'} />
                        </Form.Group>
                        <Form.Group as={Col} controlId={'price'}>
                            <Form.Label>Արժեք(դրամ)</Form.Label>
                            <Form.Control required name={'price'} type={'number'}>
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId={'description'}>
                        <Form.Label>Նկարագրություն</Form.Label>
                        <Form.Control as={'textarea'} rows={'3'} name={'description'} />
                    </Form.Group>
                    <Form.Row>
                        <Form.Group as={Col} controlId={'category'}>
                            <Form.Label>Կատեգորիա</Form.Label>
                            <Form.Control required as={'select'} onChange={this.onCategoryChange}>
                                {this.getSelectOptions(this.state.categories)}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group as={Col} controlId={'subcategory'}>
                            <Form.Label>Ենթակատեգորիա</Form.Label>
                            <Form.Control required name={'categoryId'} as={'select'}>
                                {this.getSelectOptions(this.state.subcategories)}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group as={Col} controlId="amount">
                            <Form.Label>Քանակը</Form.Label>
                            <Form.Control name={'amount'} type={'number'}>
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row className={'justify-content-center'}>
                        <Form.Group controlId={'image'}>
                            <Form.Label>Նկար</Form.Label>
                            <Form.Control required type={'file'} name={'image'} onChange={this.onUpload} />
                            <Image
                                src={defaultImage}
                                width={250}
                                height={200}
                                alt={'Ընտրեք նկար'}>
                            </Image>
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId={'submitButton'}>
                        <Col sm={{ span: 20 }}>
                            <Button type={'submit'}>Հաստատել</Button>
                        </Col>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default Admin;