﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import { changeOrderStatus } from "../store/actions/orderActions";
import { getOrders, getOrdersError, getOrdersPending } from '../store/reducers/orderReducer';
import getActiveOredrs from '../store/actions/fetchOrdersAction';

class Manager extends Component {

    componentDidMount() {
        this.props.getActiveOredrs();
    }

    handleClick = (tableNumber, address) => {
        changeOrderStatus(tableNumber, address);
    };

    render() {
        if (this.props.pending) {
            return (
                <Loader
                    type="ThreeDots"
                    color="#00BFFF"
                    height={50}
                    width={150}
                />)
        } else if (this.props.activeOrders.length === 0) {
            return (
                <div className={'container'}>
                    Պատվերներ չկան
                </div>
            )
        } else {
            var prevTableNumber = -1;
            var prevAddress = -1;
            var rowTableNumber = null;
            var rowButton = null;
            var place = "";
            return (
                <div className={'container'}>
                    <table>
                        <tbody>
                            {
                                this.props.activeOrders.map(item => {
                                    console.log(item.table_number, prevTableNumber,
                                        item.address, prevAddress);
                                    if (item.table_number == 0) {
                                        place = item.address;
                                    } else {
                                        place = `${item.table_number} - րդ սեղան`;
                                    }
                                    if (item.table_number !== prevTableNumber ||
                                        item.address !== prevAddress) {
                                        var count = this.props.activeOrders.filter(Item => (Item.table_number === item.table_number) && (Item.address === item.address)).length
                                        rowTableNumber = <td rowSpan={count} >
                                            {place}
                                        </td>
                                        rowButton = <td rowSpan={count}>
                                            <button
                                                className={'btn btn-success'}
                                                onClick={() => this.handleClick(item.table_number, item.address)}>
                                                Ավարտել
                                            </button>
                                        </td>
                                        prevTableNumber = item.table_number;
                                        prevAddress = item.address;
                                    } else {
                                        rowTableNumber = null;
                                        rowButton = null;
                                    }
                                    return (
                                        <tr key={item.id} >
                                            {rowTableNumber}
                                            <td>
                                                {item.title}
                                            </td>
                                            <td>
                                                {item.description}
                                            </td>
                                            <td>
                                                {item.quantity} հատ
                                            </td>
                                            {rowButton}
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        activeOrders: getOrders(state),
        error: getOrdersError(state),
        pending: getOrdersPending(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeOrderStatus: (tableNumber, address) => dispatch(changeOrderStatus(tableNumber, address)),
        getActiveOredrs: () => dispatch(getActiveOredrs())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Manager);