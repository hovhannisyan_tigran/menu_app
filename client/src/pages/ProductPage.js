﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProductsError, getProducts, getProductsPending } from '../store/reducers/productReducer';
import fetchProductsAction from '../store/actions/fetchProductsAction';
import ProductList from '../components/ProductList';

class ProductPage extends Component {

    render() {
        this.props.fetchProducts(this.props.match.params.id);
        return (
            <div>
                <h1 style={{ color: '#ffffff' }}>{this.props.match.params.name}</h1>
                <ProductList />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    error: getProductsError(state),
    products: getProducts(state),
    pending: getProductsPending(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchProducts: fetchProductsAction
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductPage);