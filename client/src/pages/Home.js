﻿import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import CarouselItem from '../components/CorouselItem';
import slider1 from '../images/slider1.jpg';
import slider2 from '../images/slider2.jpg';
import slider3 from '../images/slider3.jpg';
import slider4 from '../images/slider4.jpg';
import slider5 from '../images/slider5.jpg';

class Home extends Component {

    render() {
        const { history } = this.props;
        return (
            <Carousel
                autoPlay={true}
                infiniteLoop={true}
                centerMode={true}
                useKeyboardArrows={true}>
                <CarouselItem
                    id={11}
                    name={'Italiano'}
                    history={history}>
                    <img src={slider1} />
                </CarouselItem>
                <CarouselItem
                    id={23}
                    name={'Սպագետտի'}
                    history={history}>
                    <img src={slider2} />
                </CarouselItem>
                <CarouselItem
                    id={31}
                    name={'Սուշի'}
                    history={history}>
                    <img src={slider3} />
                </CarouselItem>
                <CarouselItem
                    id={41}
                    name={'Թխվածքներ'}
                    history={history}>
                    <img src={slider4} />
                </CarouselItem>
                <CarouselItem
                    id={51}
                    name={'Հյութեր'}
                    history={history}>
                    <img src={slider5} />
                </CarouselItem>
            </Carousel>
        );
    }
}

export default Home;