﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import Item from '../components/Item';
import { confirmOrder } from "../store/actions/cartActions";

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fromHome: true
        }
    }

    handleSubmit = () => {
        let cart = this.props.cart.map(item => ({ id: item.product.id, quantity: item.quantity }));
        let address = "0";
        let tableNumber = 0;
        if (this.state.fromHome) {
            address = document.getElementById('address').value;
        } else {
            tableNumber = document.getElementById('tableNumber').value;
        }
        this.props.confirmOrder(cart, tableNumber, address);
        alert("Պատվերը հաջողությամբ հաստատված է");
    };

    onChange = () => {
        this.setState({ fromHome: !this.state.fromHome });
    }

    render() {
        this.total = 0;
        this.props.cart.map(item => this.total += item.product.price * item.quantity);
        const cart = this.props.cart.length > 0 ? (
            <div>
                <div className="panel-body">
                    {
                        this.props.cart.map(item => {
                            return (
                                <div key={item.product.id}>
                                    <Item item={item} />
                                    <hr />
                                </div>
                            )
                        })
                    }
                </div>
                <div className="panel-footer">
                    <div className="row text-center">
                        <div className="col-xs-11">
                            <h4 className="text-right">Ընդամենը <strong>{this.total.toFixed(3)} դրամ</strong></h4>
                        </div>
                    </div>
                </div>
                <input type="checkbox" name="scales" checked={this.state.fromHome} onChange={this.onChange} />
                <label>Պատվիրել տնից</label>
                {address}
                <button className={'btn btn-success'} onClick={this.handleSubmit}> Հաստատել պատվերը</button>
            </div>
        ) : (
                <div className="panel-body">
                    <p>Պատվերներ չկան <a href="/">Վերադառնալ գլխավոր մենյու</a></p>
                </div>
            )

        const address = this.state.fromHome ? (
            <div className={'form-group'}>
                <label>Հասցե </label>
                <input id={'address'} className={'col-3'} placeholder={'Հասցե'} type={'text'} />
            </div>
        ) : (
                <div className={'form-group'}>
                    <label>Սեղանի համար </label>
                    <input id={'tableNumber'} className={'col-1'} placeholder={'N'} type={'number'} />
                </div>
            )
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <div className="panel panel-info">
                            <div className="panel-heading">
                                <div className="panel-title">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            <h5><FontAwesomeIcon icon={faShoppingCart} /> Պատվերներ </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {cart}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart.cart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        confirmOrder: (cart, tableNumber, address) => dispatch(confirmOrder(cart, tableNumber, address))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);