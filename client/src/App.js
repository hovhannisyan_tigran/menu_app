﻿import React from 'react';
import { Router, Switch, Route, Link } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faHome } from '@fortawesome/free-solid-svg-icons';
import Home from './pages/Home';
import Sidebar from './components/Sidebar';
import Menu from './components/Menu';
import ProductPage from './pages/ProductPage';
import Cart from './pages/Cart';
import Manager from './pages/Manager';
import Admin from './pages/Admin';
import './App.css';

function App() {
    const history = createBrowserHistory();
    return (
        <Router history={history}>
            <div className="App">
                <Sidebar 
                        content = {<Menu history={history} />}
                    >
                    <span className="navbar">
                        
                        <Link to = {"/"}>
                            <FontAwesomeIcon icon={faHome} />
                            Գլխավոր
                        </Link>
                        <Link to = {"/cart"}>
                            <FontAwesomeIcon icon={faShoppingCart} />
                            Պատվերներ
                        </Link>
                    </span>
                    <Switch>
                        <Route history={history} exact path="/" component={Home} />
                        <Route history={history} path="/products/:id/:name" component={ProductPage} />
                        <Route history={history} path="/cart" component={Cart} />
                        <Route history={history} path="/manager" component={Manager} />
                        <Route history={history} path="/admin" component={Admin} />
                    </Switch>
                </Sidebar>
                </div>
            </Router>
    );
}

export default App;