export const get = (url, successCallback, errorCallback) => {
    fetch(url)
        .then(res => res.json())
        .then(res => {
            if (res.error) {
                throw res.error;
            }
            successCallback(res);
        })
        .catch((error) => {
            errorCallback(error);
            throw error;
        });
};

export const post = (url, data) => {
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res)
        .catch((error) => {
            throw error;
        });
};

export const upload = (url, data) => {
    fetch(url, {
        method: 'POST',
        body: data
    }).then(res => res)
        .catch((error) => {
            throw error;
        });
};

export const put = (url, data) => {
    console.log(url, data);
    fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res)
        .catch((error) => {
            throw error;
        });
};