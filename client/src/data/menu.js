const menuTree = [
    {
        name: "Պիցցա",
        id: 1,
        isOpen: false, 
        children: [
            {
                name: "Italiano",
                id: 11
            },
            {
                name: "Americano",
                id: 12
            },
            {
                name: "Pizzetta",
                id: 13
            }
        ]
    },
    {
        name: "Ուտեստներ",
        id: 2,
        isOpen: false,
        children: [
            {
                name: "Րիզոտտո",
                id: 21
            },
            {
                name: "Պաստա",
                id: 22
            },
            {
                name: "Սպագետտի ",
                id: 23
            },
            {
                name: "Ապուրներ",
                id: 24
            },
            {
                name: "Տաք ուտեստներ",
                id: 25
            },
            {
                name: "Հաց ",
                id: 26
            },
            {
                name: "Ծովամթերք ",
                id: 27
            },
            {
                name: "Աղցաններ ",
                id: 28
            }
        ]
    },
    {
        name: "Սուշի Սուտեկի բար",
        id: 3,
        isOpen: false,
        children: [
            {
                name: "Սուշի",
                id: 31
            },
            {
                name: "Ռոլլեր",
                id: 32
            },
            {
                name: "Մեծ ռոլլեր",
                id: 33
            },
            {
                name: "Ասսորտի",
                id: 34
            }
        ]
    },
    {
        name: "Դեսերտ",
        id: 4,
        isOpen: false,
        children: [
            {
                name: "Թխվածքներ",
                id: 41
            },
            {
                name: "Սառը աղանդեր",
                id: 42
            },
            {
                name: "Մածուն",
                id: 43
            }
        ]
    },
    {
        name: "Ըմպելիքներ",
        id: 5,
        isOpen: false,
        children: [
            {
                name: "Հյութեր",
                id: 51
            },
            {
                name: "Թեյ",
                id: 52
            },
            {
                name: "Սուրճ ",
                id: 53
            },
            {
                name: "Ջուր",
                id: 54
            },
            {
                name: "Կոկտեյլներ",
                id: 55
            },
            {
                name: "Գարեջուր ",
                id: 56
            },
            {
                name: "Գինի ",
                id: 57
            }
        ]
    },
];

export default menuTree;