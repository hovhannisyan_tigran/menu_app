import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import Product from './Product';
import { addToCart } from "../store/actions/cartActions";
class ProductList extends Component {

    shouldComponentRender() {
        return !this.props.pending;
    }

    render() {
        if (this.shouldComponentRender()) {
            return (
                <div className="container">
                    <div className="row">
                        {
                            this.props.products.map(product => (
                                <Product
                                    key={product.id}
                                    product={product}
                                    inCart={product.inCart}
                                    addToCart={this.props.addToCart}
                                />
                            ))
                        }
                    </div>
                </div>
            )
        } else {
            return (
                <Loader
                    type="ThreeDots"
                    color="#00BFFF"
                    height={50}
                    width={150}
                />)
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        products: state.product.products,
        pending: state.product.pending,
        getProducts: ownProps.getProducts
    }
};

const mapDispatchToProps = {
    addToCart
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);