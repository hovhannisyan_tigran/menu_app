﻿import React, { Component } from 'react';
import InfinityMenu from "react-infinity-menu";
import "react-infinity-menu/src/infinity-menu.css";
import menuTree from '../data/menu';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tree: menuTree
        }
    }

    onNodeMouseClick = (event, tree) => {
        this.setState({
            tree: tree
        });
    }

    onLeafMouseClick = (event, leaf) => {
        this.props.history.push(`/products/${leaf.id}/${leaf.name}`);
        // window.location.reload();
    }

    render() {
        return (
            <InfinityMenu
                tree={this.state.tree}
                onNodeMouseClick={this.onNodeMouseClick}
                onLeafMouseClick={this.onLeafMouseClick}
            />
        );
    }
}

export default Menu;