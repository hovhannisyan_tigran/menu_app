import React from "react";
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

class CustomSidebar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false
    };
  }

  onSetSidebarOpen = (open) => {
    this.setState({ sidebarOpen: open });
  }

  render() {
    return (
      <Sidebar
        sidebar={this.props.content}
        open={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
        sidebarClassName={'sidebar'}
        sidebarDocked={true}
      >
        <Link to={'#'} className={'menuButton'}
          onClick={() => this.onSetSidebarOpen(true)}>
          <FontAwesomeIcon icon={faBars} />
                      Մենյու
          </Link>
        {this.props.children}
      </Sidebar>
    );
  }
}

export default CustomSidebar;