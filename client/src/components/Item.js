﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { updateCartQuantity, removeFromCart } from '../store/actions/cartActions';

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quantity: this.props.item.quantity,
            btnVisible: false
        };
    }

    handleChange = (e) => {
        if (e.target.value <= 0) {
            this.props.removeFromCart(this.props.item.product.id);
            return;
        }

        if (e.target.value > this.props.item.product.amount) {
            alert("Ներողություն։ Այս պահին հնարավոր չէ պատվիրել նշված քանակությամբ");
            return;
        }

        if (this.state.quantity !== e.target.value) {
            this.setState({
                quantity: e.target.value,
                btnVisible: true
            });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.updateCartQuantity(this.props.item.product.id, this.state.quantity);
        this.setState({
            btnVisible: false
        });
    }

    handleRemove = () => {
        this.props.removeFromCart(this.props.item.product.id);
    }

    render() {
        const { item } = this.props;
        return (
            <div className="row">
                <div className="col-4 col-md-2">
                    <img className="cart-item-itg"
                        alt={'Produc'}
                        src={item.product.image} />
                </div>
                <div className="col-4">
                    <h4 className="product-name"><strong>{item.product.title}</strong></h4>
                </div>
                <div className="col-6 col-md-6">
                    <div className="col-3 text-right">
                        <h6><strong>{item.product.price} <span className="text-muted">x</span></strong></h6>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="col-4 col-md-4">
                            <input type="number" className="form-control input-sm" onChange={this.handleChange} value={this.state.quantity} />
                        </div>

                        {
                            this.state.btnVisible ? (
                                <div className="col-2">
                                    <button type="submit" className="btn btn-info">Թարմացնել</button>
                                </div>
                            ) : null
                        }

                        <div className="col-2">
                            <button type="button" onClick={this.handleRemove} className="btn btn-link btn-xs">
                                <FontAwesomeIcon icon={faTrash} /></button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateCartQuantity: (productId, quantity) => dispatch(updateCartQuantity(productId, quantity)),
        removeFromCart: (productId) => dispatch(removeFromCart(productId))
    }
};

export default connect(null, mapDispatchToProps)(Item);