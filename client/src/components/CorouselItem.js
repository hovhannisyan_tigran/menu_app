import React, { Component } from 'react';
import sliderDescriptions from '../data/sliderDescriptions'
class CarouselItem extends Component {

    clickHandler = (e) => {
        this.props.history.push(`/products/${this.props.id}/${this.props.name}`);
    }

    render() {
        return (
            <div
                id={this.props.id}
                name={this.props.name}
                onClick={this.clickHandler}>
                {this.props.children}
                <div className={'carouselText'}>
                    <h1> {this.props.name} </h1>
                </div>
            </div>
        );
    }
}

export default CarouselItem;