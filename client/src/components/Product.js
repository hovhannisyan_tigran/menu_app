﻿import React, { Component } from 'react';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inCart: this.props.inCart
        }
    }

    addToCart = (e) => {
        this.props.addToCart(this.props.product);
        this.setState({ inCart: true });
    }

    render() {
        const { product } = this.props;
        return (
            <div className="col-md-4">
                <figure className="card card-product">
                    <div className="img-wrap">
                        <a href={product.image}>
                            <img className="card-img-top"
                                src={product.image}
                                alt={'Product'} />
                        </a>
                    </div>
                    <figcaption className="info-wrap">
                        <h4 className="title">{product.title}</h4>
                        <p className="desc">{product.description}</p>
                    </figcaption>
                    <div className="bottom-wrap">
                        {
                            this.state.inCart ? (
                                <span className="btn btn-sm btn-success float-right">Պատվիրված է</span>
                            ) : (
                                    <button onClick={this.addToCart} className="btn btn-sm btn-primary float-right">Պատվիրել</button>
                                )
                        }
                        <div className="price-wrap h5">
                            <span className="price-new">
                                {product.price + ' դրամ'}
                            </span>
                        </div>
                    </div>
                </figure>
            </div>
        )
    }
}

export default Product;